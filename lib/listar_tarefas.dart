import 'package:flutter/material.dart';
import 'package:lista_de_tarefas/provider/tarefas_provider.dart';
import 'package:provider/provider.dart';
import 'package:lista_de_tarefas/adicionar_tarefa.dart';

class ListaTarefa extends StatelessWidget {
  const ListaTarefa({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text(
          'Sua Lista de Tarefas',
          style: TextStyle(color: Color.fromARGB(255, 150, 39, 241)), 
        ),
        backgroundColor: Colors.black,
      ),
      body: Stack(
        children: [
          Positioned(
            top: 20.0,
            left: 20.0,
            child: ElevatedButton(
              onPressed: () async {
                final newTask = await Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const AdicionarTarefa()),
                );

                if (newTask != null) {
                  // ignore: use_build_context_synchronously
                  Provider.of<TaskProvider>(context, listen: false).addTask(newTask);
                }
              },
              child: const Text(
                'Adicione uma Tarefa',
                style: TextStyle(color: Color.fromARGB(255, 150, 39, 241)), 
              ),
            ),
          ),
          Positioned(
            top: 60.0,
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              padding: const EdgeInsets.only(top: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    'Selecione uma tarefa e arraste para a direita para deletá-la.',
                    style: TextStyle(fontSize: 15.0, color: Color.fromARGB(255, 150, 39, 241)),
                  ),
                  const SizedBox(height: 20.0),
                  Expanded(
                    child: Consumer<TaskProvider>(
                      builder: (context, taskProvider, _) {
                        return ListView.builder(
                          itemCount: taskProvider.tasks.length,
                          itemBuilder: (context, index) {
                            return Dismissible(
                              key: Key(taskProvider.tasks[index]),
                              onDismissed: (direction) {
                                Provider.of<TaskProvider>(context, listen: false).removeTask(index);
                              },
                              background: Container(
                                color: const Color.fromARGB(255, 244, 177, 54),
                                alignment: Alignment.centerRight,
                                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                                child: const Icon(Icons.delete, color: Color.fromARGB(255, 255, 0, 0)),
                              ),
                              child: Card(
                                color: Colors.green,
                                child: ListTile(
                                  title: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        taskProvider.tasks[index],
                                        style: const TextStyle(color: Colors.white),
                                      ),
                                      const Icon(Icons.arrow_forward, color: Colors.black), 
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
