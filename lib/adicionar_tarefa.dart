import 'package:flutter/material.dart';
import 'package:lista_de_tarefas/provider/tarefas_provider.dart';
import 'package:provider/provider.dart';

class AdicionarTarefa extends StatelessWidget {
  // ignore: use_key_in_widget_constructors
  const AdicionarTarefa({Key? key});

  @override
  Widget build(BuildContext context) {
    // ignore: no_leading_underscores_for_local_identifiers
    final TextEditingController _taskController = TextEditingController();

    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text('Adicione uma Tarefa', style: TextStyle(color: Color.fromARGB(255, 150, 39, 241))),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: _taskController,
                style: const TextStyle(color: Colors.white),
                decoration: const InputDecoration(
                  hintText: 'Digite aqui sua tarefa...',
                  hintStyle: TextStyle(color: Colors.white70),
                  border: OutlineInputBorder(),
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Provider.of<TaskProvider>(context, listen: false).addTask(_taskController.text);
                Navigator.of(context).pop();
              },
              child: const Icon(Icons.note_add_outlined, color: Color.fromARGB(255, 150, 39, 241)),
            ),
          ],
        ),
      ),
    );
  }
}
